import { userConstants } from './../constants/user.constants';

let user = sessionStorage.getItem('user');
const initialState = user ? { loggedIn: false, user, isLoading: false } : {};

export function GETAPPLICATIONS(state = initialState, action) {
  console.log("inside GETAPPLICATIONS function", action)
  switch (action.type) {
    case userConstants.GET_APPLICATIONS_REQUEST:
      return {
        loggingIn: true,
        user: action.user,
        isLoading : true

      };
    case userConstants.GET_APPLICATIONS_SUCCESS:
      console.log("staaaa",action)
      return {
        loggedIn: true,
        getApplicationsData: action.res,
        isLoading : false
      };
    case userConstants.GET_APPLICATIONS_FAILURE:
      return {
        isLoading : false
      };
    case userConstants.LOGOUT:
      return {};
    default:
      return state
  }
}
