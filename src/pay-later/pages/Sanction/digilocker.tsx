import React, { useContext } from "react";
import styles from "./index.module.css";
import Navbar from "../../components/molecules/Navbar";
import Footer from "../../components/molecules/Footer";
import Button from "../../components/atoms/Button";
import { DataContext } from "../../context/DataContext";
import { DataContextType } from "../../types/DataContextType";
import { useNavigate } from "react-router-dom";
import { createDigilocker } from "../../services/sanctions";
let Digio: any;

function Digilocker() {
  const { userId, applicationId, mobileNumber } = useContext(
    DataContext
  ) as DataContextType;
  const navigate = useNavigate();

  const fetchJsFromCDN = (src: any, externals: string[]) => {
    externals = !externals ? (externals = []) : externals;
    return new Promise((resolve, reject) => {
      const script = document.createElement("script");
      script.setAttribute("src", src);
      script.addEventListener("load", () => {
        resolve(
          externals.map((key: any) => {
            const ext = window[key];
            typeof ext === "undefined" &&
              console.warn(`No external named '${key}' in window`);
            return ext;
          })
        );
      });
      script.addEventListener("error", reject);
      document.body.appendChild(script);
    });
  };

  function digilockerHandler() {
    fetchJsFromCDN("https://app.digio.in/sdk/v10/digio.js", ["Digio"]).then(
      (digio: any) => {
        createDigilocker(applicationId, userId)
          .then((result) => {
            const { kid, tokenId } = result.data;

            Digio = digio[0];
            let d = new Digio({
              environment: "production",
              logo: "yourlogourl",
              theme: {
                primaryColor: "#234FDA",
                secondaryColor: "#234FDA",
              },
              is_iframe: true,
              callback: (_digio: any) => {
                console.log("ALL: ", _digio);

                if (_digio.error_code === "CANCELLED") {
                  console.log("Flow cancelled by user");
                  navigate("/sanctions/selfie");
                  return;
                }
                if (_digio.error_code !== undefined) {
                  navigate("/sanctions/selfie");
                  throw new Error(_digio.message);
                }

                navigate("/sanctions/selfie");

                console.log(_digio);
              },
            });

            console.log(d);

            d.init();
            d.submit(kid, mobileNumber, tokenId);
          })
          .catch((error) => console.log("error", error));
      }
    );
  }

  return (
    <div className={styles.body}>
      <div className={styles.backdrop}>
        <Navbar />
        <div className={styles.container}>
          <Button
            onPress={() => {
              digilockerHandler();
            }}
            text={"Digilocker"}
          />
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default Digilocker;
