import React, { useContext, useState } from "react";
import styles from "./index.module.css";
import IconIncomeDetails from "../../images/icons/income_details.svg";
import Navbar from "../../components/molecules/Navbar";
import Footer from "../../components/molecules/Footer";
import LoanStepCard from "../../components/molecules/Card";
import InputText from "../../components/atoms/InputText";
import Button from "../../components/atoms/Button";
import Label from "../../components/atoms/Label";
import { useNavigate } from "react-router-dom";
import { getApproval, updateEmployment } from "../../services/application";
import { DataContext } from "../../context/DataContext";
import { DataContextType } from "../../types/DataContextType";
import Dropzone from "react-dropzone";

function WorkDetails() {
  const [employmentType, setEmploymentType] = useState<"SALARIED" | "SELF">(
    "SALARIED"
  );

  const { userId, applicationId } = useContext(DataContext) as DataContextType;

  const [state, setState] = useState({
    employerName: "",
    salary: "",
    incomePerMonth: "",
    typeOfBusiness: "",
  });

  const navigate = useNavigate();

  const submitEmploymentDetails = () => {
    // setPanDetails(PANzoopData);
    updateEmployment({
      userId: userId,
      employmentType: employmentType,
      employerName: state.employerName,
      salary: Number(state.salary),
      incomePerMonth: Number(state.incomePerMonth),
      typeOfBusiness: state.typeOfBusiness,
    })
      .then((result) => {
        console.log(result);
        getApproval({
          applicationId,
          userId,
        })
          .then((result) => {
            console.log(result);
            if (result.res.status === "Rejected") {
              navigate("/congratulations");
            } else {
              navigate("/congratulations");
            }
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className={styles.body}>
      <div className={styles.backdrop}>
        <Navbar />
        <div className={styles.container}>
          <LoanStepCard
            // description="Permanent Address & Current Location"
            title="Income details"
            image={IconIncomeDetails}
          />
          <div>
            <div
              style={{
                padding: "1rem",
                background: "#FFF7F2",
                border: "1px solid #F9D8D6",
                borderRadius: "12px 12px 0px 0px",
                width: "100%",
              }}
            >
              <p style={{fontSize:'1.2em', fontWeight: "bold" }}>Work Details</p>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                // justifyContent: "center",
                // alignItems: "center",
                gap: "1rem",
                border: "1px solid #F9D8D6",
                background: "#FFFCFA",
                padding: "1rem",
                boxShadow: "0px 3px 3px rgba(211, 32, 40, 0.1)",
                borderRadius: "0px 0px 12px 12px",
              }}
            >
              <div  className={styles.addressInputGrid}>
                <div>
                  <p
                    style={{
                      textAlign: "center",
                      color: "#3D3D3D",
                      fontWeight: "bold",
                      fontSize:'1.2em',
                    }}
                  >
                    Employment details
                  </p>
                  <div
                    className={`${
                      employmentType === "SALARIED"
                        ? styles.employmentDetialsActive
                        : styles.employmentDetialsInactive
                    }`}
                    onClick={() => setEmploymentType("SALARIED")}
                  >
                    <p style={{fontSize:'1em'}}>Salaried</p>
                  </div>
                  <div
                    className={`${
                      employmentType === "SELF"
                        ? styles.employmentDetialsActive
                        : styles.employmentDetialsInactive
                    }`}
                    onClick={() => setEmploymentType("SELF")}
                  >
                    <p  style={{fontSize:'1em'}}>Self Employee</p>
                  </div>
                </div>
              
                {employmentType === "SALARIED" ? (
                  <>
                    <div>
                      <p style={{fontSize:'1.2em'}}>Company Name</p>
                      <InputText
                        value={state.employerName}
                        changeHandler={(e) =>
                          setState((s) => ({
                            ...s,
                            employerName: e.target.value,
                          }))
                        }
                        square
                        placeholder="Ex: FeeMonk"
                      />
                    </div>
                    <div>
                      <p style={{fontSize:'1.2em'}}>Net Annual Salary</p>
                      <InputText
                        value={state.salary}
                        changeHandler={(e) =>
                          setState((s) => ({ ...s, salary: e.target.value }))
                        }
                        square
                        placeholder="₹"
                      />
                    </div>
                  </>
                ) : (
                  <>
                    <div>
                      <p style={{fontSize:'1.2em'}}>Profession</p>
                      <InputText
                        value={state.incomePerMonth}
                        changeHandler={(e) =>
                          setState((s) => ({
                            ...s,
                            incomePerMonth: e.target.value,
                          }))
                        }
                        square
                        placeholder="Profession"
                      />
                    </div>
                    <div>
                      <p style={{fontSize:'1.2em'}}>Yearly income</p>
                      <InputText
                        value={state.typeOfBusiness}
                        changeHandler={(e) =>
                          setState((s) => ({
                            ...s,
                            typeOfBusiness: e.target.value,
                          }))
                        }
                        square
                        placeholder="₹"
                      />
                    </div>
                  </>
                )}
                <Button 
                  onPress={() => {}}
                  text={"Save"}
                  fullWidth={false}
                  secondary
                />
              </div>
              <br />
            </div>
          </div>
          <div
          
          >
            <div
              style={{
                padding: "1rem",
                background: "#FFF7F2",
                border: "1px solid #F9D8D6",
                borderRadius: "12px 12px 0px 0px",
                width: "100%",
              }}
            >
              <p style={{fontSize:'1.2em', fontWeight: "bold" }}>Bank Statement</p>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                // justifyContent: "center",
                // alignItems: "center",
                gap: "1rem",
                border: "1px solid #F9D8D6",
                background: "#FFFCFA",
                padding: "1rem",
                boxShadow: "0px 3px 3px rgba(211, 32, 40, 0.1)",
                borderRadius: "0px 0px 12px 12px",
                width: "100%",
              }}
            >
              <p style={{fontSize:'1.2em', }} >
                Uploading a bank account statement can enhance your chances of
                availing better loan amount
              </p>
              <Dropzone
                onDrop={(acceptedFiles: any) => console.log(acceptedFiles)}
              >
                {({
                  getRootProps,
                  getInputProps,
                }: {
                  getRootProps: any;
                  getInputProps: any;
                }) => (
                  <section>
                    <div
                      {...getRootProps()}
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        flexDirection: "column",
                        cursor: "pointer",
                      }}
                    >
                      <input {...getInputProps()} />
                      <svg
                        width="50"
                        height="50"
                        viewBox="0 0 57 56"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <rect
                          x="3.5"
                          y="3"
                          width="50"
                          height="50"
                          rx="25"
                          fill="#F2F4F7"
                        />
                        <rect
                          x="3.5"
                          y="3"
                          width="50"
                          height="50"
                          rx="25"
                          stroke="#F9FAFB"
                          stroke-width="6"
                        />
                        <path
                          d="M32.1586 32.0806L27.6721 27.594M27.6721 27.594L23.1855 32.0806M27.6721 27.594V37.6887M37.0826 34.7613C38.1765 34.1649 39.0408 33.2211 39.5388 32.079C40.0369 30.9369 40.1404 29.6614 39.8331 28.454C39.5257 27.2465 38.825 26.1757 37.8416 25.4107C36.8581 24.6457 35.6479 24.2299 34.4019 24.2291H32.9886C32.6491 22.916 32.0163 21.6968 31.1379 20.6634C30.2594 19.63 29.1581 18.8092 27.9167 18.2627C26.6753 17.7162 25.3262 17.4582 23.9708 17.5082C22.6154 17.5581 21.2889 17.9147 20.0912 18.551C18.8934 19.1874 17.8554 20.087 17.0554 21.1823C16.2553 22.2775 15.714 23.5399 15.472 24.8745C15.2301 26.2091 15.2938 27.5812 15.6585 28.8876C16.0231 30.194 16.6792 31.4007 17.5774 32.417"
                          stroke="#475467"
                          stroke-width="1.66667"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                      </svg>

                      <p
                        style={{
                          color: "#d32028",
                          fontSize:'1em'
                        }}
                      >
                        Click to upload
                      </p>
                      <p style={{
                          
                          fontSize:'1em'
                        }}>PDF or ZIP</p>
                    </div>
                  </section>
                )}
              </Dropzone>
            </div>
          </div>
          <Button
            text={"Save & Next"}
            onPress={() => {
              submitEmploymentDetails();
            }}
          />
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default WorkDetails;
