import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';

import { Container, List, ListItem,Button ,Grid} from '@mui/material';
import Logo from '../../Assets/images/logo.svg'
 
import {Homestyle} from './Style'
import Login from './Login'
import Verification from './Verification'
import { Link } from "react-router-dom";
export default function ButtonAppBar() {
  const [open, setOpen] = React.useState(false);
  const [open2, setOpen2] = React.useState(false);
  return (
    <>
    <Homestyle>
        <Container>
      <AppBar position="static" className='Appbar-main' >
            <Grid container >
            <Grid item xs={12} sm={4} >
              <div className='logo-cntr' >
                <Link to="/" >
            <img style={{width:'15em'}} alt='' src={Logo} className='logo'  />
            </Link>
            </div>
            </Grid>
            <Grid item xs={13} sm={8} >
            <List>

<ListItem>
    <Button style={{width:'11rem',height:'2.8rem'}} variant="contained" color='primary'  >
    Download App
  
    </Button>
</ListItem>
<ListItem>
    <Button style={{width:'8rem',height:'2.8rem'}} variant="contained" color='secondary' onClick={()=>{
      setOpen(true)
    }} >
    Login
    </Button>
</ListItem>
</List>
            </Grid>
            </Grid>
 
     
      </AppBar>
      </Container>
      </Homestyle>
      <Login open={open} setOpen={setOpen}  open2={open2} setOpen2={setOpen2} />
      <Verification open2={open2} setOpen2={setOpen2} open={open} setOpen={setOpen}  />
      </>
  );
}
