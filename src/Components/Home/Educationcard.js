import React from "react";
import {
  Container,
  List,
  ListItem,
  Button,
  Grid,
  Paper,
  Box,
  Typography,
} from "@mui/material";
import { Homestyle } from "./Style";
import { useNavigate } from "react-router-dom";

export default function Educationcard() {
  const navigate = useNavigate();
  return (
    <Homestyle>
      <div className="education-bg">
        <Container>
          <Typography  gutterBottom className="Priority">
            Your education is our Priority
          </Typography>
          <Typography  gutterBottom className="Crores">
            You can now avail Education loan
            <br /> up to <span className="text-cror"> 10 Lakhs</span>
          </Typography>
          <Button
            color="primary"
            variant="contained"
            className="applybtn"
            style={{fontSize:'1rem',width:'8em',height:'2em'}}
            onClick={() => {
              navigate(
                "/eyJtb2JpbGVOdW1iZXIiOiI4NzYzMDM5MzgwIiwiY291cnNlIjoiRGF0YSBTY2llbmNlIiwiZmVlcyI6MTAwMDAsInN0dWRlbnROYW1lIjoiU3dhcGFuZGVlcCIsImluc3RpdHV0ZU5hbWUiOiJVbmFjYWRlbXkifQ=="
              );
            }}
          >
            Apply Now
          </Button>
        </Container>
      </div>
    </Homestyle>
  );
}
