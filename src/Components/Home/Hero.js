import React from 'react'
import { Container, List, ListItem,Button ,Grid,Paper,Box,Typography} from '@mui/material';
import {Homestyle} from './Style'
import Loanicon from '../../Assets/images/Loanicon.svg'
import Guru from '../../Assets/images/Guru.svg'
import repaymentIcon from '../../Assets/images/repayment.png'
import paymentIcon from '../../Assets/images/payment.png'
import applicationIcon from '../../Assets/images/Applications.png'
import Educationcard from './Educationcard';
import Login from './Login'
import Verification from './Verification'

export default function Hero() {

  const [open, setOpen] = React.useState(false);
  const [open2, setOpen2] = React.useState(false);


  return (
    <>
    <Homestyle>
        <Container>
            <Paper className='paper-setting'  >
                {/* <div className='paper-btn' >
                <Grid container >
                    <Grid item xs={6} md={3} >
                    <Button   fullWidth color='primary'   >
                    <img src={applicationIcon} style={{width:'1em',height:'1em',marginRight:'0.5em'}}/>

<span style={{fontSize:'0.8rem'}}>Applications</span>
                    </Button>
                    </Grid>
                    <Grid item xs={6} md={3} >
                    <Button   fullWidth color='primary'   >
                    <img style={{width:'1.2em',height:'1.2em',marginRight:'0.5em'}} src={Loanicon} marginRight={1} />
<span style={{fontSize:'0.8rem'}}>Loans</span>
                    </Button>
                    </Grid>
                    <Grid item xs={6} md={3} >
                    <Button   fullWidth color='primary'   >
                  <img src={repaymentIcon} style={{width:'1em',height:'1em',marginRight:'0.5em'}}/>

<span style={{fontSize:'0.8rem'}}>Repayment</span>
                    </Button>
                    </Grid>
                    <Grid item xs={6} md={3} >
                    <Button   fullWidth color='secondary'   >
                   <img src={paymentIcon} style={{width:'1em',height:'1em',marginRight:'0.5em'}}/>



<span style={{fontSize:'0.8rem'}}>Fee Payment</span>
                    </Button>
                    </Grid>
                </Grid>
                </div> */}
                <Box textAlign="center" my={6}  >
                <img style={{width:'24vw',height:'24vw'}} src={Guru}   />
                <Typography variant="h3" gutterBottom className="payment-text" >
                No active Fee Payments
      </Typography>
      <Button className='started' >
      <span style={{fontSize:'1.5rem'}}>To get started</span>
      </Button>
      <div>
      <Button style={{fontSize:'2rem'}} variant='contained' color='primary'  className='login-btn' onClick={()=>{
      setOpen(true)
    }}>
      Login 
      </Button>
      </div>

                </Box>
                {/* edcuation crd started */}
                <Educationcard/>
       {/* edcuation crd ended */}
            </Paper>
        </Container>

    </Homestyle>
    <Login open={open} setOpen={setOpen}  open2={open2} setOpen2={setOpen2} />
    <Verification open2={open2} setOpen2={setOpen2} open={open} setOpen={setOpen}  />

    </>
  )
}
