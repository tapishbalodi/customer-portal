import React, { useState } from "react";
import { Container, Button, Grid, Paper, Box } from "@mui/material";
import { Homestyle } from "../Home/Style";
import Loanicon from "../../Assets/images/Loanicon.svg";
import Repaymenticon from "../../Assets/images/repayment-icon.png"
import Applicationicon from "../../Assets/images/Applicationicon.png";
import feepaymenticon from "../../Assets/images/Feepaymenticon.png"
import Feepayment from "./Feepayment";
import Repayments from "./Repayments";
import Loan from "./Loan";
import Applications from "./Applications";

export default function Hero() {
  const [selectedTab, setSelectedTab] = useState("APPLICATIONS");

  return (
    <>
      <Homestyle>
        <Container>
          <Paper className="paper-setting">
            <div className="paper-btn">
              <Grid container>
                <Grid item xs={6} md={4}>
                  <Button
                    fullWidth
                    color={
                      selectedTab === "APPLICATIONS" ? "secondary" : "primary"
                    }
                    onClick={() => setSelectedTab("APPLICATIONS")}
                  >
                    {/* <svg
                      width="14"
                      height="18"
                      viewBox="0 0 14 18"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M8.66797 1.51855H2.67305C2.27556 1.51855 1.89435 1.67646 1.61328 1.95752C1.33222 2.23859 1.17432 2.6198 1.17432 3.01729V15.0071C1.17432 15.4046 1.33222 15.7858 1.61328 16.0669C1.89435 16.348 2.27556 16.5059 2.67305 16.5059H11.6654C12.0629 16.5059 12.4441 16.348 12.7252 16.0669C13.0063 15.7858 13.1642 15.4046 13.1642 15.0071V6.01475M8.66797 1.51855L13.1642 6.01475M8.66797 1.51855V6.01475H13.1642M10.1667 9.76157H4.17178M10.1667 12.759H4.17178M5.67051 6.76411H4.17178"
                        stroke="black"
                        stroke-width="1.4"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg> */}
                    <Box component="img" src={Applicationicon} marginRight={1} />
                    Applications
                  </Button>
                </Grid>
                {/* <Grid item xs={6} md={4}>
                  <Button
                    fullWidth
                    color={selectedTab === "LOANS" ? "secondary" : "primary"}
                    onClick={() => setSelectedTab("LOANS")}
                  >
                    <Box component="img" src={Loanicon} marginRight={1} />
                    Loans
                  </Button>
                </Grid> */}
                {/* <Grid item xs={6} md={3}>
                  <Button
                    fullWidth
                    onClick={() => setSelectedTab("REPAYMENT")}
                    color={
                      selectedTab === "REPAYMENT" ? "secondary" : "primary"
                    }
                  >
                   
                    <Box component="img" src={Repaymenticon} marginRight={1} />
                    Repayment
                  </Button>
                </Grid> */}
                <Grid item xs={6} md={4}>
                  <Button
                    fullWidth
                    color={
                      selectedTab === "FEE_PAYMENT" ? "secondary" : "primary"
                    }
                    onClick={() => setSelectedTab("FEE_PAYMENT")}
                  >
                    {/* <svg
                      width="19"
                      height="18"
                      viewBox="0 0 19 18"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M0.943359 0.365625H0.793359V0.515625V15.6168V15.7668H0.943359H4.64169H4.79169V15.6168V15.1307V14.9807H4.64169H1.58717V1.15176H12.0565V7.2284V7.3784H12.2065H12.7003H12.8503V7.2284V0.515625V0.365625H12.7003H0.943359Z"
                        fill="#D32028"
                        stroke="#D32028"
                        stroke-width="0.3"
                      />
                      <path
                        d="M6.31703 15.1247L6.84665 13.0874L10.2321 14.3302L10.2838 14.3492L10.3355 14.3302L13.721 13.0874L14.2506 15.1214L10.2838 16.5265L6.31703 15.1247ZM10.3355 7.39786L10.2838 7.37893L10.2322 7.39786L3.45191 9.88369L3.35354 9.91976V10.0245V11.7036V11.8083L3.4519 11.8444L6.09777 12.8146L5.40018 15.4893L5.36541 15.6226L5.49532 15.6686L10.2338 17.3442L10.2839 17.3619L10.3339 17.3441L15.0724 15.6651L15.2022 15.6191L15.1674 15.4858L14.4699 12.8147L16.4203 12.1009V12.6113C16.0106 12.7655 15.7058 13.1486 15.7058 13.6136C15.7058 13.8885 15.8147 14.1362 15.9867 14.3242L15.2177 16.4202L15.1739 16.5395L15.2844 16.6023L15.4666 16.7057L15.4666 16.7057L15.468 16.7065C16.3047 17.1694 17.3297 17.1694 18.1664 16.7065L18.1664 16.7065L18.1678 16.7057L18.3499 16.6023L18.4604 16.5395L18.4167 16.4202L17.6476 14.3208C17.8196 14.133 17.9286 13.8857 17.9286 13.6136C17.9286 13.152 17.624 12.7661 17.2141 12.6113V10.0245V9.91976L17.1157 9.88369L10.3355 7.39786ZM16.8154 14.6876C16.8547 14.6876 16.8937 14.6852 16.9319 14.6806L17.4575 16.1132C17.0397 16.2592 16.5912 16.2592 16.1734 16.1132L16.6991 14.6805C16.7368 14.6851 16.7756 14.6876 16.8154 14.6876ZM16.8154 13.3189C17.0013 13.3189 17.1312 13.4576 17.1312 13.6102C17.1312 13.7628 17.0013 13.9015 16.8154 13.9015C16.6288 13.9015 16.4996 13.7655 16.4996 13.6102C16.4996 13.4576 16.6296 13.3189 16.8154 13.3189ZM4.14735 10.4671L10.2839 8.21562L16.4203 10.4638V11.2575L10.2838 13.509L4.14735 11.2609V10.4671Z"
                        fill="#D32028"
                        stroke="#D32028"
                        stroke-width="0.3"
                      />
                      <path
                        d="M8.41711 3.07888H8.26711V3.22888V3.71502V3.86502H8.41711H10.8967H11.0467V3.71502V3.22888V3.07888H10.8967H8.41711Z"
                        fill="#D32028"
                        stroke="#D32028"
                        stroke-width="0.3"
                      />
                      <path
                        d="M6.42786 4.58206H6.27786V4.73206V5.21819V5.36819H6.42786H10.8967H11.0467V5.21819V4.73206V4.58206H10.8967H6.42786Z"
                        fill="#D32028"
                        stroke="#D32028"
                        stroke-width="0.3"
                      />
                      <path
                        d="M6.42786 6.08535H6.27786V6.23535V6.72149V6.87148H6.42786H10.8967H11.0467V6.72149V6.23535V6.08535H10.8967H6.42786Z"
                        fill="#D32028"
                        stroke="#D32028"
                        stroke-width="0.3"
                      />
                    </svg> */}
                    <Box component="img" src={feepaymenticon} marginRight={0.5} />
                   <span style={{whiteSpace:'nowrap',fontFamily:'Inter-Medium'}}>Fee Payment</span> 
                  </Button>
                </Grid>
              </Grid>
            </div>
            {selectedTab === "FEE_PAYMENT" ? (
              <Feepayment />
            ) : selectedTab === "REPAYMENT" ? (
              <Repayments />
            ) : selectedTab === "LOANS" ? (
              <Loan />
            ) : (
              <Applications />
            )}
          </Paper>
        </Container>
      </Homestyle>
    </>
  );
}

