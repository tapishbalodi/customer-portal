import React from "react";
import Navbar from "./Navbar";
import Hero from "./Hero";

import Footer from "../Home/Footer";
export default function Index() {
  return (
    <div className="home-bg ">
      <Navbar />
      <Hero />
      <Footer />
    </div>
  );
}
